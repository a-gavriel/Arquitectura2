#include <string>
#include <iostream>
#include <thread>
#include <queue>
#include <string>
#include <iomanip>
using namespace std;

// The function we want to execute on the new thread.
void task1(string msg)
{
    cout << "task1 says: " << msg;
}

int main()
{
	// Constructs the new thread and runs it. Does not block execution.
	//thread t1(task1, "Hello");

	// Do other things...

	// Makes the main thread wait for the new thread to finish execution, therefore blocks its own execution.
	//t1.join();
	/*
	std::queue<int> myqueue;
  int myint;

  std::cout << "Please enter some integers (enter 0 to end):\n";

  do {
    std::cin >> myint;
    myqueue.push (myint);
  } while (myint);

  std::cout << "myqueue contains: ";
  while (!myqueue.empty())
  {
    std::cout << ' ' << myqueue.front();
    myqueue.pop();
  }
  std::cout << "\n ---------";
*/
	int x = 15;
	int xx = x%8;
	std::cout  << std::setw(30) << "This is left aligned"
     << std::right << std::setw(30) << "This is right aligned\n";
  return 0;
}
