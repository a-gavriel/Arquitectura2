



#include <string>
#include <iostream>
#include <thread>
#include <fstream>

#include <mutex>
std::mutex mu;

using namespace std;

void task1(std::string msg, int id){
	std::lock_guard<std::mutex> locker(mu);
	std::cout << msg << "thread: " << id << std::endl;

}

// 

int operations[100][2];

void readdata(){
	std::ifstream infile("data0.txt");
	int a, b;
	int counter = 0;
	while (infile >> a >> b){
		operations[counter][0] = a;
		operations[counter][1] = b;
		++counter;        
	}
	for (int i = 0;i<100;++i){
		std::cout << operations[i][0] << " " << operations[i][1] << std::endl;
	}
}

int main()
{
	// Constructs the new thread and runs it. Does not block execution.
	thread t1(task1, "Hello",1);

	// Do other things...
	readdata();

	// Makes the main thread wait for the new thread to finish execution, therefore blocks its own execution.
	t1.join();
}
