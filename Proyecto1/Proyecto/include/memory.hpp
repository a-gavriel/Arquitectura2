#ifndef _MEMORY_H
#define _MEMORY_H

#include <mutex>

class Memory {
  private:
    char data[16];
    std::mutex memoryM;
  public:
    Memory();
    void write (int,char);
    char read (int);
    void debug();
};

#endif
