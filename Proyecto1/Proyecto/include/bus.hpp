#ifndef _BUS_H
#define _BUS_H

#include <mutex>

class Bus {
  private:
    char operation = ' ';
    int index = 0;
    char value = ' ';
    std::mutex busM;
  public:
    bool locked = 0;
    void write (int,char);
    char read (int);
    char mem(char,int,char);
    void debug();
};

#endif
