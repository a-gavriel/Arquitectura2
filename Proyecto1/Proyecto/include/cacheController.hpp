#ifndef _CACHECONTROLLER_H
#define _CACHECONTROLLER_H

#include "cache.hpp"

class CacheController {
  private:
    Cache mycache;
    int id;
    char idC;
    
    char write(int,char,int,char);
    char read(int,char,int,char);
  public:
    int find(int);
    char mem(int,char,int,char);
    CacheController(int);
    void checkbus();
    void debug();
};

#endif
