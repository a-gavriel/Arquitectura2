#ifndef _PROCESSOR_H
#define _PROCESSOR_H

#include "cacheController.hpp"
#include <string>
class Processor {
  private:
    std::string _pr;
    int id;
    char idC;
    int pc = 0;
    int operation = 0;
    int index = 0;
    char value = ' ';
    int operations[100][2];    
    void write (int,char);
    char read (int);    
    void operate();
    CacheController mycacheController;
  public:
    Processor(int number);
    int loadData();
    void process();
    void debug();
};

#endif
