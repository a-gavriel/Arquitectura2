#ifndef _BUSCONTROLLER_H
#define _BUSCONTROLLER_H

#include <queue>
#include <thread>
#include <mutex>

struct request{
  int processor;
  char operation;
  int index;
  char value;
  request(int,char,int,char);
  request();
};

class BusController {
  private:
    std::mutex busCM;
    std::queue<request> reqQueue;
    char resultReq = ' ';
    int  resultProc = -1;
    
  public:
    
    int askBus();
    char mem(int,char,int,char);
    void debug();
};

#endif
