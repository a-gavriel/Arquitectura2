#ifndef _TIMECONST_H
#define _TIMECONST_H

static const int clockt = 1000000;
static const int second = 1000000;
static const int timeMemory = 800000;
static const int timeBus = 300000;
static const int timeProcessor = 1000000;
static const int timeCache = 40000;


#endif