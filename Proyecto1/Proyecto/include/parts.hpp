
#include "memory.hpp"
#include "bus.hpp"
#include "processor.hpp"
#include "busController.hpp"
#include "cacheController.hpp"
#include "cache.hpp"
#include "cacheBus.hpp"
#include <mutex>
#include <iostream>

extern BusController _busController;
extern bool manualClock;
extern bool _clock;
extern Bus _bus;
extern Memory _memory;
extern Processor _processor0;
extern Processor _processor1;
extern Processor _processor2;
extern Processor _processor3;

extern CacheBus _cacheBus;

