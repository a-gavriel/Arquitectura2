#ifndef _CACHEBUS_H
#define _CACHEBUS_H

#include <thread>
#include <mutex>

struct cData{
  int id;
  char op;
  int index;
  char value;
  //cData();
};

class CacheBus {
  private:
    std::mutex writeM, readM;
    cData busData;
    char resultReq = ' ';
    int  resultProc = -1;
    
  public:
    
    int canread = 0;
    void postnew(int,char,int,char);
    cData readbus();
    int readable();
};


#endif