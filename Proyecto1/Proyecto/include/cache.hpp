#ifndef _CACHE_H
#define _CACHE_H

class Cache {
  public:
    int id;
    char idC;
    char data[8];
    int  tag[8];
    int  valid[8];
    int find(int);
    void cast();
  
    Cache(int);
    void write (int,char);
    char read (int);
    void debug();
};

#endif
