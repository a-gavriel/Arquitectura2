#include "../include/cache.hpp"
#include "../include/timeconst.hpp"
#include "../include/parts.hpp"
#include "../include/utils.hpp"
#include "../include/cacheBus.hpp"

#include <thread>
#include <unistd.h>
#include <iostream>
#include <string>

Cache::Cache(int id ){
	this->id = id;
  this->idC = (char)(id + 48);
  std::string printing = " ";
  printing += idC;
  
  for(int i = 0; i < 8; ++i){
		data[i] = '-';
		valid[i] = 0;
    tag[i] = 0;
		
	}
}


int Cache::find(int index){	
	usleep(timeCache/2);
  std::string ch = "ch";
	ch += idC;
  std::string printing = "";
  
  int mapped = index%8;
  if(valid[mapped]){
    if(tag[mapped]== index){
      printing = "Hit on: ";
      printing += std::to_string(index);
      Println(ch,printing);
      return 1;
    }
    printing = "Missed on tag: ";
    printing += (char)(index+48);
    Println(ch,printing);
    return -2;
  }
  printing = "Missed on invalid: ";
  printing += (char)(mapped+48);
  Println(ch,printing);
  return -1;  
}

void Cache::write (int index,char value){	
	usleep(timeCache);
  int mapped = index%8;
	data[mapped] = value;
  tag[mapped] = index;
  valid[mapped] = 1;
  debug();
}

char Cache::read (int index){	
	usleep(timeCache);
  int mapped = index%8;
  char result = data[mapped];
  debug();
	return result;
}

void Cache::debug(){	
  std::string ch = "ch";
	ch += idC;
	char currentValue = ' ';
  int currentDir = 0;
  int currentValid = 0;
	std::string printing = "   0  1  2  3  4  5  6  7\nValues: ";
  std::string pValues = "" ;
  std::string pValid = "" ;
  std::string pTags = "" ;
	for(int i = 0; i < 8; ++i){
		pValues += data[i];
    pTags += std::to_string(tag[i]);
    pValid += std::to_string(valid[i]);
    pValues += "  " ;
    pTags += "  " ;
    pValid += "  " ;
	}
  printing += pValues + "\nValids: " + pValid + "\nTags:   " + pTags;
	Println(ch,printing);
}


