#include "../include/memory.hpp"
#include "../include/timeconst.hpp"
#include "../include/parts.hpp"
#include "../include/utils.hpp"

#include <thread>
#include <unistd.h>
#include <iostream>
#include <string>

Memory::Memory(){
	for(int i = 0; i < 16; ++i){
		data[i] = '-';    
	}
}

void Memory::write (int index,char value){
	debug();
	std::string printing = "writting in: " + std::to_string(index) + " - " + value;
	Println("mem",printing);
	std::lock_guard<std::mutex> locker(memoryM);
	usleep(timeMemory);
	Memory::data[index] = value;
	debug();
}

char Memory::read (int index){
	debug();
	std::string printing = "reading in: " + std::to_string(index);
	Println("mem",printing);
	std::lock_guard<std::mutex> locker(memoryM);
	usleep(timeMemory);
	char value =  Memory::data[index];
	debug();
	return value;	
}

void Memory::debug(){
	char value = ' ';
	std::string printing = "0 1 2 3 4 5 6 7 8 9 A B C D E F\nData ";
	
	for(int i = 0; i < 16; ++i){
		value = data[i];
		printing += value;
		printing += ' ';
	}
	
	Println("mem",printing);
	
}


	
	