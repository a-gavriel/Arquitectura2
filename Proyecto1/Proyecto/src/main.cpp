#include <iostream>
#include <ostream>
#include <fstream>
#include <stdlib.h> 
#include <mutex>
#include <ncurses.h>
#include <thread>
#include <unistd.h>
#include <string>


#include "../include/memory.hpp"
#include "../include/bus.hpp"
#include "../include/busController.hpp"
#include "../include/cacheBus.hpp"
#include "../include/cacheController.hpp"
#include "../include/cache.hpp"
#include "../include/parts.hpp"
#include "../include/timeconst.hpp"
#include "../include/utils.hpp"

/*
/// opening terminals
int exit_status0 = system("gnome-terminal");
int exit_status1 = system("gnome-terminal");
int exit_status2 = system("gnome-terminal");
int exit_status3 = system("gnome-terminal");
int exit_status4 = system("gnome-terminal");
int exit_status5 = system("gnome-terminal");


/// defining terminals
std::ofstream term0("/dev/pts/2", std::ios_base::out);
std::ofstream term1("/dev/pts/3", std::ios_base::out);
std::ofstream term2("/dev/pts/4", std::ios_base::out);
std::ofstream term3("/dev/pts/5", std::ios_base::out);
*/



// definition of global variables
/// processors
Processor _processor0(0);
Processor _processor1(1);
//Processor _processor2(2);
//Processor _processor3(3);
void ff0(){_processor0.process();}
void ff1(){_processor1.process();}
//void ff2(){_processor2.process();}
//void ff3(){_processor3.process();}

///other components
Bus _bus;
Memory _memory;
CacheBus _cacheBus;
BusController _busController;
bool _clock = 0;
bool manualClock = 0;



//function used to change the manual clock with spacebar
void fMclock(){
  char ch;
  system("stty -echo"); // supress echo
  system("stty cbreak"); // go to RAW mode
  // ch = getchar(); // or something like that
  while ((ch = getchar()) != 'q') {
    if (ch == ' '){
      manualClock = (!manualClock);
    }
  }
  system ("stty echo"); // Make echo work
  system("stty -cbreak");// go to COOKED mode
}

//function used to change the clock every N time
void fclock(){
  while (1) {
    usleep(timeProcessor);
    _clock = (!_clock);
  }
}

///bus controller
void bController(){
  _busController.askBus();
}


int main(){

  //std::thread t1(ff);
  std::thread tclock1(fMclock);
  std::thread tclock2(fclock);
  std::thread thReqest(bController);
  thReqest.detach();
  
  std::thread t0(ff0);
  std::thread t1(ff1);
  //std::thread t2(ff2);
  //std::thread t3(ff3);
  
  
  
  

  t0.join();
  t1.join();
  //t2.join();  
  //t3.join();
  
  
  

  return 0;
}

///// Do not brake it with Crtl-C. If you did so restore echo so
// "stty echo" in blind mode. Echo supressed.