import numpy as np
from random import randrange
from random import uniform


for FILE in range(4):
	def deleteContent(pfile):
		pfile.seek(0)
		pfile.truncate()

	operations = 100
	operationMean = randrange(3) 
	operationSigma = 1# uniform(0,2)
	s = np.random.normal(operationMean, 1, operations)
	data = [0] * operations
	for i in range(len(s)):
		if (s[i]) < 0.0:
			data[i] = 0
		elif 0.0 < (s[i]) < 1.0:
			data[i] = 1
		elif 1.0 < (s[i]):
			data[i] = 2

	ss = ""
	print("operations:",operationMean , operationSigma,">\t")

	for i in range(3):
		ss += (str)(i) + "-" + (str)(data.count(i)) + " "
	print(ss)



	num_access = data.count(2) + data.count(1)
	access = [0]* num_access
	accessMean = randrange(16)  
	accessSigma = uniform(2,8)

	s_access = np.random.normal(accessMean, accessSigma, num_access)

	for i in range(num_access):
		value = s_access[i]
		if value < 0.0:
			access[i] = 0
		elif value > 15.0:
			access[i] = 15
		else:
			access[i] = int(value)

	print("access:",num_access, accessMean , accessSigma,">\t")
	for i in range(16):
		ss += (str)(i) + "-" + (str)(access.count(i)) + " "
	print(ss)

	ss = ""
	wr_c = 0
	for i in range(operations):
		op = data[i]
		if (op) == 0:
			ss += str(0) + " 0\n"
		elif op == 1:
			ss += str(1) + " " + str(access[wr_c]) + "\n"
			wr_c += 1
		else:
			ss += str(2) + " " + str(access[wr_c]) + "\n"
			wr_c += 1
		
	print(" ---------------- ")
	#print(ss)



	fname = "data" + str(FILE) + ".txt"
	f= open(fname,"w+")
	deleteContent(f)
	f.write(ss)
	f.close()