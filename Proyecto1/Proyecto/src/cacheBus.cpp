#include "../include/cacheBus.hpp"
#include "../include/timeconst.hpp"
#include "../include/parts.hpp"
#include "../include/utils.hpp"


#include <thread>
#include <unistd.h>
#include <iostream>
#include <string>


void CacheBus::postnew(int processor, char operation,int index, char value){
  std::lock_guard<std::mutex> locker(writeM);
  canread = 0;
  usleep(timeCache/100);
  busData.id = processor;
  busData.op = operation;
  busData.index = index;
  busData.value = value;
  std::string printing = "index: " + std::to_string(index) + " value: " ;
  printing += value;
  Println("ChB",printing);
  canread = 1;
}

int CacheBus::readable(){
  
  usleep(timeCache/10000);
  
  return 1;
}

cData CacheBus::readbus(){
  
  //cData result = busData;
  return busData;
}
