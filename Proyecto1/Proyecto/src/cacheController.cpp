#include "../include/cache.hpp"
#include "../include/cacheController.hpp"
#include "../include/timeconst.hpp"
#include "../include/parts.hpp"
#include "../include/utils.hpp"
#include "../include/cacheBus.hpp"

#include <thread>
#include <unistd.h>
#include <iostream>
#include <string>

CacheController::CacheController(int _id) : mycache(_id) {
  this->id = _id;
	this->idC = (char)(_id + 48);
  
}

int CacheController::find(int index){	
	usleep(timeCache);
  int mapped = index%8;
  if(mycache.valid[mapped]){
    if(mycache.tag[mapped]== index){
      return 1;
    }
    return -2;
  }
  return -1;  
}

void CacheController::checkbus(){
  
  int Btag,Ctag;
  
  cData data = _cacheBus.readbus();
  if((data.id != id)&&(data.op == 'w')){
    Btag = data.index;
    Ctag = mycache.tag[Btag%8];
    if(Btag == Ctag){
      mycache.valid[Btag%8] = 0;
      std::string cc = "cc";
      cc += idC;
      std::string printing = "Invalidating from bus position: ";
      printing += (char)(Btag%8 + 48 );
      Println(cc,printing);
    }
  }

}

char CacheController::write(int id, char op,int index,char value){
  char result = ' ';
  int hit = mycache.find(index);
  if(hit != -2){
    mycache.write(index,value);
  }else{
    result = mycache.read(index);
    //not needed since it's write-through
    //_busController.mem(id,op,index,result);
    mycache.write(index,value);
  }
  
  mycache.valid[index] = 1;
  
  ::_cacheBus.postnew(id,op,index,value);
  // Write-Through
  ::_busController.mem(id,op,index,value);
  return ' ';
}

char CacheController::read(int id, char op,int index,char value){
  int hit = mycache.find(index);
  char result = ' ';
  if(hit > 0){
    result = mycache.read(index);
  }else{
    result = _busController.mem(id,op,index,value);
    mycache.write(index,value);
  }
  
  // Write-Through
  
  return result;
}

char CacheController::mem(int id, char op,int index, char value){
  char result = ' ';
  
  checkbus();
  if(op == 'w'){
    result = write(id,op,index,value);
  }
  if(op == 'r'){
    result = read(id,op,index,value);
  }
  return result;
}