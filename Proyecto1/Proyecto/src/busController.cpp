#include "../include/bus.hpp"
#include "../include/timeconst.hpp"
#include "../include/parts.hpp"
#include "../include/utils.hpp"
#include "../include/busController.hpp"

#include <thread>
#include <unistd.h>
#include <iostream>
#include <string>

request::request(int processor, char operation,int index, char value){
  this->operation = operation;
  this->processor = processor;
  this->index = index;
  this->value = value;
}
request::request(){
  this->operation = 0;
  this->processor = 0;
  this->index = 0;
  this->value = 0;
}


char BusController::mem(int processor, char operation,int index,char value){
	debug();
  request newReq(processor,operation,index,value);
  reqQueue.push(newReq);
  char result;
  while(processor != resultProc){
    usleep(timeBus);
  }
  resultProc = -1;
	result = resultReq;
  resultReq = ' ';
	debug();
	return result;
}

int BusController::askBus(){
  while(1){
    request frontReq;
    if((!reqQueue.empty())&& (!_bus.locked)){    
      frontReq = reqQueue.front();    
      resultReq =  _bus.mem(frontReq.operation,frontReq.index,frontReq.value);
      resultProc = frontReq.processor;
      reqQueue.pop();    
    }
  }
  return 0;
}



void BusController::debug(){
  std::string printing = "";
  long unsigned int size = reqQueue.size();
  printing += "size: " + std::to_string(size);
  Println("bsC",printing);
}