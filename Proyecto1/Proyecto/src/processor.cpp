#include "../include/processor.hpp"
#include "../include/timeconst.hpp"
#include "../include/parts.hpp"
#include "../include/utils.hpp"


#include <pthread.h>
#include <unistd.h>
#include <iostream>

#include <mutex>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <fstream>
#include <string>

Processor::Processor(int number) : mycacheController(number){
	this->id = number;
	this->idC = (char)(number + 48);	
	loadData();
	
}

int Processor::loadData(){
	usleep(1000000);
	std::string pr = "pr";
	pr += idC;
	Println(pr,"loading data");
	char filename[] = "dataX.txt";
	filename[4] = idC;
	std::ifstream infile(filename);
	int a, b;
	int counter = 0;
	while ((infile >> a >> b) && (counter < 100)){
		operations[counter][0] = a;
		operations[counter][1] = b;
		++counter;        
	}
	infile.close();
	return 0;
}

void Processor::write(int position, char value){
	//calls controller
	std::string pr = "pr";
	pr += idC;
	Println(pr,"writing to " + std::to_string(position));
	mycacheController.mem(id,'w',position,idC);
	Println(pr,"---wrote in " + std::to_string(position));
}

void Processor::process(){ 
	std::string pr = "pr";
	pr += idC;
	Println(pr,"processing data");
	pc = 0;
	
	int  currentOp, currentDir;
	while (pc < 100){
		usleep(timeProcessor);
		if (_clock && manualClock){
			
			currentOp = operations[pc][0];
			currentDir = operations[pc][1];	
			Println(pr,"***current pc: " + std::to_string(pc));
			Println(pr,"current op: " + std::to_string(operations[pc][0]));
			if(currentOp == 0){
				operate();
			}
			if(currentOp == 1){
				char value = read(currentDir);
			}
			if(currentOp == 2){
				write(currentDir,idC);
			}

			++pc;
		}
	}
	

}

void Processor::operate(){	
	usleep(timeProcessor);
	std::string pr = "pr";
	pr += idC;
	Println(pr,"---processing");
}

char Processor::read(int position){
	std::string pr = "pr";
	pr += idC;
	Println(pr,"reading from: " + std::to_string(position));
	char value = mycacheController.mem(id,'r',position,idC);
	std::string printing = "---read ";
	printing += value;
	Println(pr,printing);
	return value;
}

void Processor::debug(){
	char value = ' '; 
}