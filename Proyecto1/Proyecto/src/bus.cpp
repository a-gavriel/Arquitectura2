#include "../include/bus.hpp"
#include "../include/timeconst.hpp"
#include "../include/parts.hpp"
#include "../include/utils.hpp"

#include <thread>
#include <unistd.h>
#include <iostream>
#include <string>

char Bus::mem(char operation,int index,char value){
	std::lock_guard<std::mutex> locker(busM);
	this->locked = 1;
	this->operation = operation;
	this->index = index;
	this->value = value;
	debug();
	usleep(timeBus);
	char result = ' ';
	if (operation == 'r'){result = ::_memory.read(index);}
	if (operation == 'w'){::_memory.write(index, value);}
	this->locked = 0;
	debug();
	return result;
}

void Bus::write (int index,char value){
	std::lock_guard<std::mutex> locker(busM);
	usleep(timeBus);    
	_memory.write(index,value);
}

char Bus::read (int index){
	std::lock_guard<std::mutex> locker(busM);
	usleep(timeBus);    
	_memory.read(index);
	return value;
}

void Bus::debug(){
	std::string printing = "";
	printing = std::to_string(index) + " - " + value ;
	Println("bus",printing);
}

