#Arquitectura de Computadores II
##Taller 1

Para compilar:
```
gcc taller1.c -lpthread 
```

También se puede utilizar el Makefile ejecutando:
```
make
```

Para ejecutar:
```
./a.out
```
