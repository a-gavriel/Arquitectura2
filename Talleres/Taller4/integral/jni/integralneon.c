#include <stdio.h>
#include "arm_neon.h"
#define SIZE 10000

static float a = 2.001;
static float x [SIZE] ;
static float y [SIZE] ;

void doprint4(float*);

float fn(float x){
  return 1/(x+1)+x/10;
}

int main (){
  float start = 1;
  float end = 400;
  float step = (end-start)/SIZE;
  float halfStep = step/2.0;

  float counter = start + halfStep;
  float newx;
  float res = 0;
  for (int i = 0; i < SIZE; ++i){
    newx = counter+step*i;
    x[i] = newx;
    y[i] = fn(newx);
  }

  float32x4_t Y, r1, r2, r3;

  float temp[4] = {0,0,0,0};

  for (int i = 0; i< SIZE; i+=4){
    // load 4 data elements at a time
    Y = vld1q_f32(y+i);
    // do the computations

    //result =  _mm_dp_ps(Y, _mm_set1_ps(step),0xff);

    r1 = vmulq_f32(Y, vdupq_n_f32(step));
    r2 = vaddq_f32(r1, r1);
    r3 = vaddq_f32(r2, r2);
    
    // store the results
    vst1q_f32(temp, r3);
    res += temp[0];
    
  }
  printf("%.15f \n", res); 


}

void doprint4(float* data){

  printf("temp *********************** \n");    
  printf("%f \t", data[0]);       
  printf("%f \t", data[1]);   
  printf("%f \t", data[2]); 
  printf("%f \t", data[3]);
  printf("\n");
}
