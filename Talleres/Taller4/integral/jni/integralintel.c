
#include <stdio.h>
#include <emmintrin.h> //v3
#include <smmintrin.h> //v4
#include <xmmintrin.h> 

#define SIZE 100

static float a = 2.001;
static float x [SIZE] ;
static float y [SIZE] ;

void doprint4(float*);

float fn(float x){
  return 1/(x+1)+x/10;
}

int main (){
  float start = 1;
  float end = 4;
  float step = (end-start)/SIZE;
  float halfStep = step/2.0;

  float counter = start + halfStep;
  float newx;
  float res = 0;
  for (int i = 0; i < SIZE; ++i){
    newx = counter+step*i;
    x[i] = newx;
    y[i] = fn(newx);
  }

  __m128 Y, result;

  float temp[4] = {0,0,0,0};

  for (int i = 0; i< SIZE; i+=4){
    // load 4 data elements at a time
    Y = _mm_loadu_ps(y+i);
    // do the computations

    result =  _mm_dp_ps(Y, _mm_set1_ps(step),0xff);
    
    // store the results
    _mm_storeu_ps(temp, result);
    res += temp[0];
    
  }
  printf("%.15f \n", res); 


}

void doprint4(float* data){

  printf("temp *********************** \n");    
  printf("%f \t", data[0]);       
  printf("%f \t", data[1]);   
  printf("%f \t", data[2]); 
  printf("%f \t", data[3]);
  printf("\n");
}
