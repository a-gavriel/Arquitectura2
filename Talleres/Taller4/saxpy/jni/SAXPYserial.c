#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#define SIZE 10000000

static const float a = 3.1415;
static float x[SIZE];
static float y[SIZE];

int main (){
	int i;
	float sum = 0.0;
	
    for (i=0;i<SIZE;++i){
        x[i] = 0.0001;
        y[i] = 0.0001;
    }
    double start_time, run_time;
    start_time = omp_get_wtime();

    for (int i = 0; i< SIZE; ++i){
        y[i] += a*x[i];		
    }    
	run_time = omp_get_wtime() - start_time;
	printf("calculated %f in %f seconds \n",y[0],run_time);
}


