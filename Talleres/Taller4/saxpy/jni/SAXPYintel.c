
#include <stdio.h>
#include <emmintrin.h> //v3
#include <smmintrin.h> //v4
#include <xmmintrin.h> 

#define SIZE 8//1000000000

static float a = 2.001;
static float x [SIZE] ;
static float y [SIZE] ;

int main (){

	for (int i = 0; i < SIZE; ++i){
		x[i] = i;
		y[i] = i;
	}

	__m128 X, Y, result;



	for (int i = 0; i< SIZE; i+=4){
		// load 4 data elements at a time
    X = _mm_loadu_ps(x+i);
    Y = _mm_loadu_ps(y+i);
    // do the computations

    result = _mm_add_ps(Y, _mm_mul_ps(X, _mm_set1_ps(a)));
    //result = _mm_dp_ps(X,Y,0xff);
    // store the results
    _mm_storeu_ps(y+i, result);

    
	}


	printf("Result *********************** \n");		
	for (int i = 0; i < SIZE; ++i){			
		printf("%2.4f \t", y[i] );
	}

	printf("\n");


}

