#include <stdio.h>
#include "arm_neon.h"

#define SIZE 8//1000000000

static float a = 2.001;
static float x [SIZE] ;
static float y [SIZE] ;

void doprint4(float*);

int main (){

    for (int i = 0; i < SIZE; ++i){
        x[i] = i;
        y[i] = i;
    }

    float32x4_t X, Y, result;

    for (int i = 0; i< SIZE; i+=4){
        // load 4 data elements at a time
    X = vld1q_f32(x+i);
    Y = vld1q_f32(y+i);
    // do the computations
    result = vaddq_f32(Y, vmulq_f32(X, vdupq_n_f32(a)));
    // store the results
    vst1q_f32(y+i, result);

    //doprint4(y+i);
    
    }

    printf("Result *********************** \n");        
    for (int i = 0; i < SIZE; ++i){         
        printf("%f \t", y[i] );
    }
    printf("\n");
}

void doprint4(float* data){
    printf("temp *********************** \n");      
    printf("%f \t", data[0]);               
    printf("%f \t", data[1]);       
    printf("%f \t", data[2]);   
    printf("%f \t", data[3]);
    printf("\n");
}