// SSE headers
#include <emmintrin.h> //v3
#include <smmintrin.h> //v4

#include <stdio.h>

union dataf{
  float f;
  int i;
};

int main(){
//****Nota: este código requiere SSE4****//
printf("Hola Mundo desde SSE \n");

float mat [4][4] = {
  1,0,1,1,
  1,1,1,1,
  1,1,1,1,
  1,1,1,1};

float mat2 [4][4] = {
  1,0,3,4,
  1,2,3,4,
  1,2,3,4,
  0,2,3,4};


union dataf mat3 [4][4];

__m128 vector1, vector2, r1, r2, r3;
union dataf data;
for(int i = 0;i<4;i++){
  vector1 = _mm_set_ps(mat[i][3],mat[i][2],mat[i][1],mat[i][0]); // Little endian, stored in 'reverse'
 
  for(int j = 0;j<4;j++){ 
    
    vector2 = _mm_set_ps(mat2[3][j],mat2[2][j],mat2[1][j],mat2[0][j]);
   
      r1 = _mm_mul_ps(vector1, vector2);
      r2 = _mm_hadd_ps(r1, r1);
      r3 = _mm_hadd_ps(r2, r2);
      mat3[i][j].i = _mm_extract_ps(r3,0);
      printf("%f \t", mat3[i][j].f);
  }
  printf("\n");
}


return 1;
}
