// SSE headers
#include <emmintrin.h> //v3
#include <smmintrin.h> //v4

#include <stdio.h>

int main(){
//****Nota: este código requiere SSE4****//
printf("Hola Mundo desde SSE \n");

short mat [16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

short shortVal;
for(int i = 0;i<8;i++){
  printf("Enter an element for the first row (%d remaining): ",8-i);
  scanf("%hi", &shortVal);
  mat[i] = shortVal;
}
for(int i = 0;i<8;i++){
  printf("Enter an element for the second row (%d remaining): ",8-i);
  scanf("%hi", &shortVal);
  mat[8+i] = shortVal;
}

__m128i vector1 = _mm_set_epi16(mat[7],mat[6],mat[5],mat[4],mat[3],mat[2],mat[1],mat[0]); // Little endian, stored in 'reverse'
__m128i vector2 = _mm_set_epi16(mat[15],mat[14],mat[13],mat[12],mat[11],mat[10],mat[9],mat[8]);

// Addition
__m128i result = _mm_max_epi16(vector1, vector2); // result = max(vector1, vector 2)

//Vector Printing
int data = 0;
int i;
printf("Result *********************** \n");
data = _mm_extract_epi16(result,0); 
printf("%hi \t", data);
data = _mm_extract_epi16(result,1); 
printf("%hi \t", data);
data = _mm_extract_epi16(result,2); 
printf("%hi \t", data);
data = _mm_extract_epi16(result,3); 
printf("%hi \t", data);
data = _mm_extract_epi16(result,4); 
printf("%hi \t", data);
data = _mm_extract_epi16(result,5); 
printf("%hi \t", data);
data = _mm_extract_epi16(result,6); 
printf("%hi \t", data);
data = _mm_extract_epi16(result,7); 
printf("%hi \t", data);


printf("\n");
return 1;
}
