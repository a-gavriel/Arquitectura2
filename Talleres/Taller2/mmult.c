#include <stdio.h>
#include <omp.h>

#define N 800
static int mat1[N][N];   
static int mat2[N][N]; 
static int result[N][N]; 

double start_time, run_time;
void multiply(int mat1[][N], int mat2[][N], int result[][N]) { 
    #pragma omp parallel for shared(mat1, mat2, result)
    for (int i = 0; i < N; i++) { 
        for (int j = 0; j < N; j++) { 
            int temporal = 0;
            for (int k = 0; k < N; k++) 
                temporal += mat1[i][k]*mat2[k][j];
            result[i][j] = temporal;
        } 
    } 
} 
int main(){    
    int i,j;
    for (i = 0; i < N; i++){ 
        for (j = 0; j < N; j++){
           mat1[i][j] = i+j; 
           mat2[i][j] = i+j;
        }        
    } 
    start_time = omp_get_wtime();
    multiply(mat1, mat2, result);
    run_time = omp_get_wtime() - start_time; 
    printf("matrix multiplication calculated in %f\n",run_time);
    return 0;
}

